const getCube = 2 ** 3
console.log(`The cube of 2 is ${getCube}`)

const address = ["258", "Washington Ave", "NW", "California", 90011]
const [streetNumber, avenue, state, city, zip] = address;
console.log(`I live at ${streetNumber} ${avenue} ${state}, ${city} ${zip}.`)

const animal = {
	name: 'Lolong',
	weight: 1075,
	feet: 20,
	inch: 3
}
const {name, weight, feet, inch} = animal;
console.log(`${name} was a saltwater crocodile. He weighed at ${weight} kgs with a measurement of ${feet} ft ${inch} in`)

const number = [1, 2, 3, 4, 5];
number.forEach ( (number) => {
	console.log(number)
})

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog()
myDog.name = "Cali";
myDog.age = 3;
myDog.breed = "Siberian Husky";
console.log(myDog)